//
//  ViewController.swift
//  AllGithubUser
//
//  Created by Rajvi on 12/08/21.
//

import UIKit


class UserTableViewCell: UITableViewCell {

    @IBOutlet var nameLable: UILabel!
    @IBOutlet var imageUser: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class ViewController: UIViewController {
   
     enum TableSection: Int {
         case userList
         case loader
     }
    @IBOutlet var indeictor: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
   
    private let pageLimit = 25
      private var currentLastId: Int? = nil
      
      private var users = [User]() {
          didSet {
              DispatchQueue.main.async { [weak self] in
                  self?.tableView.reloadData()
              }
          }
      }
      
      override func viewDidLoad() {
          super.viewDidLoad()
          setupView()
          fetchJSONData()
      }
      
      private func setupView() {
          title = "Github Users"
          tableView.rowHeight = 125
          tableView.dataSource = self
          tableView.delegate = self
      }
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if let destination = segue.destination as? UserDetailsView,
         let indexPath = tableView.indexPathForSelectedRow {
        destination.selectedAuteur = users[indexPath.row]
        
      }
    }
     
      private func fetchJSONData(completed: ((Bool) -> Void)? = nil) {
          GithubAPIJSON.shared.getUsers(perPage: pageLimit, sinceId: currentLastId) { [weak self] result in
              switch result {
              case .success(let users):
                  self?.users.append(contentsOf: users)
                  self?.currentLastId = users.last?.id
                  completed?(true)
              case .failure(let error):
                  print(error.localizedDescription)  
                  completed?(false)
              }
          }
      }
  }

  extension ViewController: UITableViewDataSource, UITableViewDelegate {
      func numberOfSections(in tableView: UITableView) -> Int {
          return 2
      }
      
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          guard let listSection = TableSection(rawValue: section) else { return 0 }
          switch listSection {
          case .userList:
              return users.count
          case .loader:
              return users.count >= pageLimit ? 1 : 0
          }
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = TableSection(rawValue: indexPath.section) else { return UITableViewCell() }
        
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserTableViewCell
        switch section {
        case .userList:
            let item = users[indexPath.row]
          print("-----item-----\(item)")
          cell.nameLable?.text = item.name
            cell.nameLable?.textColor = .label
          let urlString = item.avatarUrl
    //      print(urlString as Any)
          let url = URL(string: urlString)
          let data = try? Data(contentsOf: url!)


          if let imageData = data {
            cell.imageUser.layer.cornerRadius = cell.imageUser.frame.size.width / 2
            ;

            let image = UIImage(data: imageData)

            cell.imageUser.image = image
          }else {
            cell.imageUser.image = UIImage(named: "circle")
          }
//              cell.detailTextLabel?.text = "\(indexPath.row + 1)"
        case .loader:
              indeictor.startAnimating()
        }
        return cell
      }
      
      func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          guard let section = TableSection(rawValue: indexPath.section) else { return }
          guard !users.isEmpty else { return }

          if section == .loader {
              print("load new data..")
              fetchJSONData { [weak self] success in
                  if !success {
                      self?.hideBottomLoader()
                  }
              }
          }
      }
      private func hideBottomLoader() {
          DispatchQueue.main.async {
              let lastListIndexPath = IndexPath(row: self.users.count - 1, section: TableSection.userList.rawValue)
              self.tableView.scrollToRow(at: lastListIndexPath, at: .bottom, animated: true)
          }
      }
  }

